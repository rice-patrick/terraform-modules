variable "function_name" {
  description = "The name of the lambda function."
}
variable "function_file" {
  description = "The file to upload as the lambda function"
}
varuable "method_name" {
  description = "The method to invoked in the lambda"
}
variable "environment_variables" {
  description = "The environment variables to set for the lambda"
  type = map(string)
}

############
# Variables with defaults
############
variable "schedule" {
  description = "How often the lambda should run. Can either be a cron syntax or a 'rate' syntax. See terraform for more."
  default = "rate(1 minute)"
}