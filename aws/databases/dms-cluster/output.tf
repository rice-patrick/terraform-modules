
output "dms_security_group_id" {
  value = aws_security_group.dms_securirity_group.id
}

output "source_endpoint_arn" {
  value = aws_dms_endpoint.source_endpoint.endpoint_arn
}

output "target_endpoint_arn" {
  value = aws_dms_endpoint.target_endpoint.endpoint_arn
}