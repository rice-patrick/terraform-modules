# Create a new replication subnet group
resource "aws_dms_replication_subnet_group" "subnet_group" {
  replication_subnet_group_description = "DMS Subnet group for ${var.job_name}"
  replication_subnet_group_id          = "dms-instance-${var.job_name}"

  subnet_ids = data.aws_subnet.subnets.*.id

}

# Create the subnet group for the cluster, using the IDs determined from tags
data "aws_subnet" "subnets" {
  count = length(var.subnet_tag_values)

  vpc_id = var.vpc_id

  filter {
    name = "tag:${var.subnet_tag_name}"
    values = [var.subnet_tag_values[count.index]]
  }
}

# Restrict the security group to the traffic incoming to the database
resource "aws_security_group" "dms_securirity_group" {
  name        = "${var.job_name}-sg"
  description = "Allows outbound traffice for DMS Instances for ${var.job_name}"
  vpc_id      = var.vpc_id

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


# Create a new replication instance (note - IAM roles from ./iam_roles.tf) are used here
resource "aws_dms_replication_instance" "replication_instance" {
  allocated_storage            = var.storage_amount
  apply_immediately            = var.apply_immediately
  auto_minor_version_upgrade   = true

  engine_version               = var.dms_engine_version

  # Set this to multi-az if the subnet group contains more than one subnet. They should be in different AZs
  multi_az                     = length(data.aws_subnet.subnets.*.id) > 1 ? true : false
  preferred_maintenance_window = var.maintenance_window
  publicly_accessible          = false # Yep, that's staying hard-coded.
  replication_instance_class   = var.instance_type
  replication_instance_id      = "replication-instance-for-${var.job_name}"
  replication_subnet_group_id  = aws_dms_replication_subnet_group.subnet_group.id


  vpc_security_group_ids = [
    aws_security_group.dms_securirity_group.id
  ]
}

data "aws_ssm_parameter" "source_database_password" {
  name = var.source_endpoint_password_path
}

resource "aws_dms_endpoint" "source_endpoint" {
  database_name               = var.source_endpoint_database_name
  endpoint_id                 = "${var.job_name}-source-endpoint"
  endpoint_type               = "source"
  engine_name                 = var.source_endpoint_database_type

  password                    = data.aws_ssm_parameter.source_database_password.value
  port                        = var.source_endpoint_database_port
  server_name                 = var.source_endpoint_database_url
  ssl_mode                    = var.source_endpoint_database_ssl

  username = var.source_endpoint_database_username
}

data "aws_ssm_parameter" "target_database_password" {
  count = var.target_endpoint_password_path != "" ? 1 : 0

  name = var.source_endpoint_password_path
}

resource "aws_dms_endpoint" "target_endpoint" {
  database_name               = var.target_endpoint_database_name
  endpoint_id                 = "${var.job_name}-target-endpoint"
  endpoint_type               = "target"
  engine_name                 = var.target_endpoint_database_type

  password                    = var.target_endpoint_password == "" ? data.aws_ssm_parameter.target_database_password[0].value : var.target_endpoint_password
  port                        = var.target_endpoint_database_port
  server_name                 = var.target_endpoint_database_url
  ssl_mode                    = var.target_endpoint_database_ssl

  username = var.target_endpoint_database_username
}