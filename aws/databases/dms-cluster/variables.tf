variable "create_iam_roles" {
  default = true
  description = "Whether or not create the DMS specific IAM roles. These are name-specific, so you can only create them once per account"
}
variable "job_name" {
  description = "The name of the DMS Job. This will be used to set up the replication instance and jobs."
}
variable "subnet_tag_name" {
  default = "GenericId"
  description = "The tag name to search for the subnet_tag_values. Shouldn't need to change in 99% of cases."
}
variable "subnet_tag_values" {
  type = list(string)
  description = "A list of subnet tags that we should use to create the Redshift cluster."

  # We're assuming subnets will be tagged with a "subnet name". This is so we don't need to reference IDs which
  # require manual configuration on each environment. It also allows decoupling of terraform configs.
  default = ["private-4-az-1", "private-4-az-2"]
}
variable "vpc_id" {
  description = "The VPC to use for creating the instance"
}

# Note as well, https://docs.aws.amazon.com/dms/latest/userguide/CHAP_BestPractices.html#CHAP_BestPractices.SizingReplicationInstance is good for this investigation
variable "instance_type" {
  description = "The size of instance to use. See https://docs.aws.amazon.com/dms/latest/userguide/CHAP_ReplicationInstance.html"
  default = "dms.t2.micro"
}
variable "maintenance_window" {
  default = "sun:10:30-sun:14:30"
  description = "Maintenance window for updates and applying changes"
}
variable "dms_engine_version" {
  default = "3.1.3"
  description = "The version of the DMS engine to use. "
}
variable "apply_immediately" {
  default = true
  description = "whether to wait for the next maintenance window to apply changes"
}

variable "storage_amount" {
  default = 20
  description = "how much storage to allocate"
}

####################################
# Source endpoint variables
####################################

variable "source_endpoint_database_name" {
  description = "Database name for the source database"
}
variable "source_endpoint_password_path" {
  description = "Path in parameter store for the password for the source database"
}
variable "source_endpoint_database_type" {
  default = "postgres"
  description = "The database engine for the source database. Note - this doesn't create a database. "
}
variable "source_endpoint_database_port" {
  default = "5432"
  description = "Port for the database"
}
variable "source_endpoint_database_url" {
  description = "URL for the host of the database"
}
variable "source_endpoint_database_ssl" {
  default = "none"
  description = "Whether or not the database connection will attempt to use SSL. Won't work for JDBC"
}
variable "source_endpoint_database_username" {
  description = "Username information for the source database"
}


####################################
# target endpoint variables
####################################


variable "target_endpoint_database_name" {
  default = "dev"
  description = "Database name for the target database"
}
variable "target_endpoint_password_path" {
  default = ""
  description = "Path in parameter store for the password for the target database"
}
variable "target_endpoint_password" {
  default = ""
  description = "The password to use when passing in the password without parameter store."
}
variable "target_endpoint_database_type" {
  default = "redshift"
  description = "The database engine for the target database. Note - this doesn't create a database. "
}
variable "target_endpoint_database_port" {
  default = "5439"
  description = "Port for the database"
}
variable "target_endpoint_database_url" {
  description = "URL for the host of the database"
}
variable "target_endpoint_database_ssl" {
  default = "none"
  description = "Whether or not the database connection will attempt to use SSL. Won't work for JDBC"
}
variable "target_endpoint_database_username" {
  description = "Username information for the target database"
}
