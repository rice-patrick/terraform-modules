
resource "aws_rds_cluster" "postgresql" {
  cluster_identifier      = "${var.cluster_name}-${var.usage}"
  engine                  = "aurora-postgresql"
  engine_mode             = "serverless"
  availability_zones      = local.azs

  database_name           = var.database_name
  master_username         = var.database_username
  master_password         = local.my_password

  backup_retention_period = var.backup_retention_period
  preferred_backup_window = var.backup_window
  skip_final_snapshot     = var.skip_final_snapshot

  vpc_security_group_ids  = [aws_security_group.database_security_group.id]
  db_subnet_group_name    = aws_db_subnet_group.subnet_group.name

  storage_encrypted       = true
  kms_key_id              = aws_kms_key.kms.arn

  apply_immediately       = true

  port = var.database_port

  tags = {
    Name     = "${var.cluster_name}-${var.usage}"
    Usage    = var.usage
  }

  scaling_configuration {
    auto_pause               = var.scaling_pause_database
    max_capacity             = var.scaling_max_capacity
    min_capacity             = var.scaling_min_capacity
    seconds_until_auto_pause = var.scaling_seconds_before_auto_pause
    timeout_action           = var.scaling_timeout_action
  }

  # Since we are dynamically determining AZs from our subnets, we need to ignore
  # changes on the AZ or the database will be destroyed every apply. If the subnets
  # change, then the destroy will still happen and include the AZ changes.
  lifecycle {
    ignore_changes = [
      availability_zones
    ]
  }
}

resource "aws_secretsmanager_secret" "database_password" {
  name_prefix = "${var.cluster_name}-database-password"
}

resource "aws_secretsmanager_secret_version" "database_password_secret_version" {
  secret_id = aws_secretsmanager_secret.database_password.id
  secret_string = aws_rds_cluster.postgresql.master_password
}

resource "aws_db_subnet_group" "subnet_group" {
  name        = "${var.cluster_name}-db-subnet-group"
  description = "${var.cluster_name}-db-subnet-group"
  subnet_ids  = var.database_subnet_ids
}

resource "aws_db_parameter_group" "aurora_db_postgres96_parameter_group" {
  name        = "${var.cluster_name}-db-param-group"
  family      = "aurora-postgresql9.6"
  description = "${var.cluster_name} database parameter group"
}

resource "aws_security_group" "database_security_group" {
  name        = "${var.cluster_name}-security-group"
  description = "Security group for the ${var.cluster_name} database"

  # Grab the VPC from the first subnet
  vpc_id      = data.aws_subnet.my_subnets[0].vpc_id
}

resource "aws_kms_key" "kms" {
  description             = "${var.cluster_name}-kms"
  deletion_window_in_days = 10

  enable_key_rotation = true
}

resource "aws_kms_alias" "aurora_db" {
   name          = "alias/${var.usage}/rds/${var.cluster_name}"
   target_key_id = aws_kms_key.kms.key_id
}


data "aws_subnet" "my_subnets" {
  count = length(var.database_subnet_ids)
  id    = var.database_subnet_ids[count.index]
}

resource "random_password" "password" {
  length = 16
  special = false
  min_numeric = 1
  min_lower = 1
  min_upper = 1
}

locals {
  my_password = var.database_password == "" ? random_password.password.result : var.database_password
  azs = distinct(data.aws_subnet.my_subnets.*.availability_zone)
}

