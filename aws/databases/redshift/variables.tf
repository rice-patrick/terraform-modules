variable "vpc_id" {
  description = "The VPC to use for creating the instance"
}
variable "cluster_name" {
  description = "The name of the instance to create."
}
variable "database_name" {
  default = "dev"
  description = "The name of the database to create within the instance. If this is not specified, uses the redshift default of 'dev'"
}
variable "username" {
  default = "admin"
  description = "The username to use for the database. If this is not specified, defaults to 'admin'"
}
variable "password" {
  default = ""
  description = "The password to use for the database. If this is not specified, a random password will be created."
}
variable "database_size" {
  default = "dc2.large"
  description = "The machine type to use for launching the cluster. Defaults to the smallest possible size."
}
variable "multi_node" {
  default = true
  description = "Whether or not to run the database on multiple nodes"
}
variable "ingress_port" {
  default = "5439"
  description = "The port to accept incoming traffic on. If not specified, defaults to redshift's 5439"
}
variable "ingress_cidr" {
  type = list(string)
  default = ["10.0.0.0/24"]
  description = "The CIDR range to allow access for. Defaults to a private IP range."
}
variable "node_count" {
  default = "2"
  description = "How many nodes to use. If 'multi_node' is set to false, this must be 1. Defaults to 2."
}
variable "skip_final_snapshot" {
  default = false
  description = "Whether or not to create a final snapshot when destroying the database."
}
variable "additional_iam_roles" {
  type = list(string)
  default = []
  description = "Additional IAM roles to associate to the cluster. Will be concatenated to the default IAM role. Max of 9 (10 - 1 default)."
}
variable "subnet_tag_name" {
  default = "GenericId"
  description = "The tag name to search for the subnet_tag_values. Shouldn't need to change in 99% of cases."
}
variable "subnet_tag_values" {
  type = list(string)
  description = "A list of subnet tags that we should use to create the Redshift cluster."

  # We're assuming subnets will be tagged with a "subnet name". This is so we don't need to reference IDs which
  # require manual configuration on each environment. It also allows decoupling of terraform configs.
  default = ["private-3-az-1", "private-3-az-2"]
}