# Create the KMS key for the efs drive
resource "aws_kms_key" "efs-encryption-key" {
  description = "KMS key EFS data storage for ${var.cluster-name}"

  enable_key_rotation = true

  tags = {
    Name  = "kms-data-efs-${var.usage}-for-${var.cluster-name}"
    Usage = var.usage
  }
}

resource "aws_efs_file_system" "efs-data-drive" {

  encrypted = true
  kms_key_id = aws_kms_key.efs-encryption-key.arn

  tags = {
    Name  = "data-efs-${var.usage}-for-${var.cluster-name}"
    Usage = var.usage
  }
}

# This is kind of wild syntax, but we're building a map below of
# AZ -> list<Subnet>. Since Targets can only exist one per AZ, we
# only create the target based on the first subnet in the map.
resource "aws_efs_mount_target" "efs-mount-target" {
  for_each = local.efs_target_subnet_map

  file_system_id = aws_efs_file_system.efs-data-drive.id
  subnet_id =  each.value[0]

  security_groups = [aws_security_group.ecs-cluster-security-group.id]
}

resource "aws_efs_access_point" "efs-access-point" {
  file_system_id = aws_efs_file_system.efs-data-drive.id
}

data "aws_subnet" "efs_subnets" {
  count = length(var.ecs_subnets)
  id = element(var.ecs_subnets, count.index)
}

locals {
  efs_target_subnet_map = {
    for subnet in data.aws_subnet.efs_subnets:
      subnet.availability_zone => subnet.id...
  }
}

