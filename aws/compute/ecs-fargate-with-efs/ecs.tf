
locals {
  non-spot-percentage = 100 - var.fargate-spot-percentage
}

resource "aws_ecs_cluster" "cluster" {
  name = "${var.cluster-name}-${var.usage}"

  capacity_providers = ["FARGATE_SPOT", "FARGATE"]

  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight            = var.fargate-spot-percentage
  }

  default_capacity_provider_strategy {
    capacity_provider = "FARGATE"
    weight            = local.non-spot-percentage
    base              = var.base-on-demand-container-count
  }

  setting {
    name = "containerInsights"
    value = "enabled"
  }

  tags = {
    Name     = "${var.cluster-name}-${var.usage}"
    Usage    = var.usage
  }
}

resource "aws_ecs_service" "ecs-service" {
  name = "${var.cluster-name}-${var.usage}"
  task_definition = aws_ecs_task_definition.service.arn

  cluster = aws_ecs_cluster.cluster.id

  desired_count = var.desired-container-count

  deployment_minimum_healthy_percent = var.deployment-min-percent
  deployment_maximum_percent = var.deployment-max-percent

  capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight            = var.fargate-spot-percentage
  }

  capacity_provider_strategy {
    capacity_provider = "FARGATE"
    weight            = local.non-spot-percentage
    base              = var.base-on-demand-container-count
  }

  #launch_type = "FARGATE"
  platform_version = "1.4.0" # Required

  load_balancer {
    target_group_arn = aws_lb_target_group.target-group.arn
    container_name   = var.container-name
    container_port   = var.container-port
  }
  # odd that this isn't in the load balancer block, but eh.
  health_check_grace_period_seconds = var.container-startup-time

  network_configuration {
    subnets = var.ecs_subnets
    security_groups = [aws_security_group.ecs-cluster-security-group.id]
  }

  lifecycle {
    ignore_changes = [
      desired_count
    ]
  }

  # Since there is no direct reference to the listener, the task will attempt to create immediately after the LB TG
  # is created, which results in a race condition where it will occassionally fail because the TG isn't associated
  # to a LB yet.
  depends_on = [aws_lb_listener.http-lb-listener, aws_lb_listener.https-lb-listener]
}

# Ensure the ECS Service can auto scale
resource "aws_appautoscaling_target" "service_target" {
  max_capacity = var.max-container-count
  min_capacity = var.min-container-count
  resource_id  = "service/${aws_ecs_cluster.cluster.name}/${aws_ecs_service.ecs-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_security_group" "ecs-cluster-security-group" {
  name = "${var.cluster-name}-sg-${var.usage}"
  description = "security group for the ecs cluster ${var.cluster-name}"
  vpc_id = var.vpc_id

  # Allow egress to the internet here
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Note, we're not creating ingress rules here, because they're created in the "load balancer" file.

  tags = {
    Name  = "${var.cluster-name}-sg-${var.usage}"
    Usage = var.usage
  }
}

# Within the same SG allow all traffic (helps with the EFS attachment significantly)
resource "aws_security_group_rule" "ingress-from-same-sg-ecs-cluster" {
  security_group_id = aws_security_group.ecs-cluster-security-group.id

  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  source_security_group_id = aws_security_group.ecs-cluster-security-group.id
}

# If we're accessing from a private subnet within our VPC CIDR, then allow
# traffic directly to the containers. Otherwise it will have to come from the LB.
# Note: The LB rule is created within the load balancer file.
resource "aws_security_group_rule" "allow-ingress-from-private-subnet" {
  security_group_id = aws_security_group.ecs-cluster-security-group.id

  type = "ingress"
  from_port = var.container-port
  to_port = var.container-port
  protocol = "-1"
  cidr_blocks = ["10.0.0.0/8"]
}
