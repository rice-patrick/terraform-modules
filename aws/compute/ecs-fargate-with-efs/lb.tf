resource "aws_lb" "alb" {
  name               = "lb-for-${var.cluster-name}-${var.usage}"
  internal           = var.lb-internal
  load_balancer_type = "application"

  security_groups    = [aws_security_group.load-balancer-security-group.id]
  subnets = var.lb-subnets

  tags = {
    Name        = "lb-for-${var.cluster-name}-${var.usage}"
    Usage       = var.usage
  }
}

resource "aws_lb_target_group" "target-group" {
  name         = "lb-tg-for-${var.cluster-name}-${var.usage}"
  target_type  = "ip" # You have to use IP here, because fargate awsvpc uses ENIs instead of instances
  port         = var.container-port
  protocol     = var.lb-container-protocol

  vpc_id       = var.vpc_id

  stickiness {
    type = "lb_cookie" # the only option
    cookie_duration = var.lb-sticky-session-duration
    enabled = var.lb-sticky-session
  }

  tags = {
    Name  = "lb-tg-for-${var.cluster-name}-${var.usage}"
    Usage = var.usage
  }

  health_check {

    path    = var.lb-health-check-url
    port    = "traffic-port"
    timeout = var.lb-health-check-timeout

    interval = var.lb-health-check-interval
    healthy_threshold   = var.lb-health-check-healthy-threshold
    unhealthy_threshold = var.lb-health-check-unhealthy-threshold

    matcher = var.lb-health-check-match-string
  }
}

resource "aws_lb_listener" "https-lb-listener" {
  count = var.lb-listener-port == 443 ? 1 : 0

  load_balancer_arn = aws_lb.alb.arn
  port = var.lb-listener-port
  protocol = "HTTPS"
  
  # TODO: Add a locals block to switch this on based on protocol
  ssl_policy = var.lb-listener-ssl-policy
  certificate_arn = var.lb-ssl-certificate-arn

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.target-group.arn
  }
}

resource "aws_lb_listener" "http-lb-listener" {
  count = var.lb-listener-port != 443 ? 1 : 0

  load_balancer_arn = aws_lb.alb.arn
  port = var.lb-listener-port
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.target-group.arn
  }
}


resource "aws_security_group" "load-balancer-security-group" {
  name = "load-balancer-security-group-${var.cluster-name}-${var.usage}"
  description = "security group for the ${var.cluster-name} load balancer"
  vpc_id = var.vpc_id

  ingress {
    to_port = var.lb-listener-port
    from_port = var.lb-listener-port
    protocol = "TCP"

    cidr_blocks = ["10.0.0.0/8"]
  }

  ingress {
    to_port = var.lb-listener-port
    from_port = var.lb-listener-port
    protocol = "TCP"

    cidr_blocks = var.lb-allowed-ips
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "load-balancer-security-group-${var.cluster-name}-${var.usage}"
    Usage = var.usage
  }
}

resource "aws_security_group_rule" "load-balancer-to-ecs-rule" {
  security_group_id = aws_security_group.ecs-cluster-security-group.id

  type = "ingress"
  protocol = "TCP"
  from_port = 49153 # Docker ports
  to_port = 65535

  source_security_group_id = aws_security_group.load-balancer-security-group.id

}


resource "aws_security_group_rule" "load-balancer-to-ecs-container-port-rule" {
  security_group_id = aws_security_group.ecs-cluster-security-group.id

  type = "ingress"
  protocol = "TCP"
  from_port = var.container-port
  to_port = var.container-port

  source_security_group_id = aws_security_group.load-balancer-security-group.id

}
