variable "usage" {
  default     = "dev"
  description = "The environment level for the resource"
}

variable "region" {
  default     = "us-east-1"
  description = "The region to use"
}

#####################
# Cluster Variables
#   These variables modify the creation of the cluster itself.
#   Right now that basically means capacity provider stuff.
#####################
variable "fargate-spot-percentage" {
  default = "90"
  description = "Value between 0 and 100 that determines what % of tasks to launch as spot tasks"
}

variable "base-on-demand-container-count" {
  default = "1"
  description = "How many containers to run 'on-demand' at a minimum. The rest are subject to the percentages."
}

variable "max-container-count" {
  default = "10"
  description = "how many containers to run max. Note that to increase past the minimum, you will need to define an aws_appautoscaling_policy in your config."
}

variable "min-container-count" {
  default = "1"
  description = "how many containers to run min"
}

#####################
# Container Variables
#   These variables control information about the container definition
#   and deployment.
#####################
variable "container-name" {
  default = "my-container"
  description = "The name of the container in the container definition"
}

variable "container-memory" {
  default = 1024
  description = "Memory for the task"
}

variable "container-cpu" {
  default = 512
  description = "Cpu units for the task"
}

variable "container-privileged" {
  default = false
  description = "whether your task def has to run as privileged. When in doubt, leave false for security reasons."
}

variable "container-port" {
  type = number
  default = 8080
  description = "the port that the container listens on"
}

variable "container-environment" {
  type = list
  default = []
  description = "environment variables to use when running the container"
}

variable "container-secrets" {
  type = list
  default = []
  description = "environment variables to inject from secrets manager when running the container"
}

variable "container-startup-time" {
  type = number
  default = 60
  description = "How long it takes your container to start up. This will be the registration delay on the LB."
}

variable "container-image" {
  description = "the container image to run. Is combined with the container tag to determine version."
}

variable "container-tag" {
  description = "the tag of the image to run. Does not default to 'latest' as that can lead to deployment inconsistencies"
}

variable "container-volume-name" {
  default = "service-drive"
  description = "Name to use within the container and task def for the mount point"
}

variable "container-volume-path" {
  default = "/data"
  description = "Where to mount the drive within the container"
}

variable "desired-container-count" {
  default = 1
  description = "how many containers should run"
}

variable "deployment-min-percent" {
  default = 50
  description = "The min % of containers to keep healthy during a deployment"
}

variable "deployment-max-percent" {
  default = 150
  description = "How many containers in % to burst during deployment"
}

variable "docker-tag" {
  default     = "latest"
  description = "the tag of the docker image to use. Defaults to latest, but you should really use an immutable tag."
}

variable "cluster-name" {
  description = "The name to use for cluster resources. Used in more places than just the cluster."
}

variable "container-log-retention-duration" {
  type = number
  default = 7
  description = "The number of days to save logs for"
}

variable "soft-ulimit-nofile" {
  type = number
  default = 1024
  description = "The soft limit for the ulimit nofile value"
}

variable "hard-ulimit-nofile" {
  type = number
  default = 4096
  description = "The hard limit for the ulimit nofile value"
}

#####################
# Networing Variables
#   As the name implies.
#####################

variable "vpc_id" {
  description = "The ID of the VPC to create resources within"
}

variable "ecs_subnets" {
  type        = list
  description = "Subnets to use for running the fargate tasks"
}

###############
# EFS Variables
#   Names and variables for the EFS drive
###############

# This section left purposefully blank. Most of what's required here can
# be derived.

###############
# Load Balancer Variables
###############
variable "lb-subnets" {
  type        = list
  description = "Subnets to use for running the load balancer"
}

variable "lb-health-check-url" {
  description = "the URL to hit when performing a health check."
}

variable "lb-health-check-timeout" {
  type = number
  default = 30
  description = "the duration of time to wait for a successful response"
}
variable "lb-health-check-interval" {
  type = number
  default = 35
  description = "how often to attempt the health check"
}

variable "lb-health-check-healthy-threshold" {
  type = number
  default = 2
  description = "how many times health check succeeds for a target to be healthy"
}

variable "lb-health-check-unhealthy-threshold" {
  type = number
  default = 5
  description = "how many times health check succeeds for a target to be not healthy"
}

variable "lb-health-check-match-string" {
  type = string
  default = "200-299"
  description = "The string range of http codes for the health check to listen for"
}

# TODO : Add support for automatically allocating an SSL certificate for domain of choice
variable "lb-listener-port" {
  default = 443
  description = "the port to listen on for traffic"
}

variable "lb-sticky-session" {
  default = false
  description = "Whether to enable sticky sessions on the LB."
}

variable "lb-sticky-session-duration" {
  default = 10800 # 3 days
  description = "how long in seconds the sticky session should last for"
}

variable "lb-listener-protocol" {
  default = "HTTPS"
  description = "DEPRECATED: the port to listen on for traffic. This is now inferred by the listener port instead. 443 will default to https, 80 will default to http"
}

# Reference: https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html#describe-ssl-policies
variable "lb-listener-ssl-policy" {
  default = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  description = "Policy string to use for the SSL Ciphers on the LB"
}

variable "lb-internal" {
  default = true
  description = "Whether the lb will be an internal lb"
}

variable "lb-container-protocol" {
  # This was previously set to HTTP hard-coded, so it has to default to this for backwards-compatibility.
  default = "HTTP"
  description = "The protocol the load balancer uses to "
}

variable "lb-ssl-certificate-arn" {
  # This has to default to blank because it's conditionally required only if HTTPS is the used protocol.
  default = ""
  description = "ARN of the ACM certificate to use for the LB listener"
}

variable "lb-allowed-ips" {
  default = ["0.0.0.0/0"]
  type = list(string)
  description = "ports that can talk to the load balancer"
}


