variable "cluster_name" {
  description = "The name of the autoscaling group. Will be used in tags throughout resources as well."
}
variable "placement_strategy" {
  description = "The strategy for placement of virtual machines on the underlying hardware."
  default = "cluster"
}
variable "usage" {
  description = "The usage level of the environment"
}
variable "max_instances" {
  description = "The maximum number of instances that are allowed in the group"
  default = 4
}
variable "min_instances" {
  description = "The smallest number of instances that are allowed in the group"
  default = 1
}
variable "desired_instances" {
  description = "How many instances to provision when the ASG is created. WILL BE IGNORED ON EVERY FUTURE DEPLOYMENT."
  default = 1
}
variable "force_delete" {
  description = "Whether to destroy the ASG before all instances are drained. Setting this to true will force kill any jobs on the instances on deleting."
  default = "false"
}
