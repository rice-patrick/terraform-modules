import boto3
import os


def lambda_handler(event, context):
    # Pull variables from our runtime
    queue_url = os.environ['QUEUE_URL']
    service = os.environ['SERVICE_NAME']
    cluster = os.environ['CLUSTER_NAME']

    # Create our clients that we'll use to calculate info
    cloudwatch = boto3.client('cloudwatch')

    # Retrieve how many messages are currently visible in the queue
    queue = boto3.resource('sqs').Queue(queue_url)
    messages = queue.attributes['ApproximateNumberOfMessages']
    print(messages)

    # Retrieve how many running tasks we have in our service
    ecs = boto3.client('ecs')
    running_tasks = ecs.describe_services(
        cluster=cluster,
        services=[service]
    )['services'][0]['runningCount']
    print(running_tasks)

    # If there is at least 1 running task, divide messages by tasks and store the metric
    if running_tasks > 0:
        message_per_server: float = int(messages) / int(running_tasks)

        response: object = cloudwatch.put_metric_data(
            MetricData=[
                {
                    'MetricName': 'messages_per_service',
                    'Dimensions': [
                        {
                            'Name': 'SERVICE',
                            'Value': service
                        },
                        {
                            'Name': 'QUEUE',
                            'Value': queue_url
                        },
                    ],
                    'Unit': 'None',
                    'Value': message_per_server
                },
            ],
            Namespace='SQS'
        )

        return response


lambda_handler("", "")
