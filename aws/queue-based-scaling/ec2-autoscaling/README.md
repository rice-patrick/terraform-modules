# PC With SubnetsV

Almost every application within AWS uses some form of Virtual Private Cloud functionality, and the creation of a VPC
with accompanying subnets is an extremely common use-case. This module provisions a very basic VPC along with public
and private subnets for use in applications. 

## Resources Created

This module creates several resources:
 - A VPC
 - A secondary CIDR range on the VPC (if set to true)
 - A variable number of public subnets, tagged with their number
 - A variable number of private subnets, tagged with their number
 
## Example usage

```hcl-terraform
module "my_vpc" {
  # Reference by Git URL so that these examples work elsewhere if someone wants to use them.
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/queue-based-scaling/ecs-service/"

  assign_secondary_block = "true"
}
```
## Variables

See the [variables.tf](./variables.tf) file for defaults and details on variables.