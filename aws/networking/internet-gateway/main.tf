variable "vpc_id" {
  description = "The VPC to create the internet gateway for"
}
variable "usage" {
  description = "The level of environment the internet gateway serves"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = var.vpc_id

  tags = {
    vpc = var.vpc_id
    usage = var.usage
  }
}
