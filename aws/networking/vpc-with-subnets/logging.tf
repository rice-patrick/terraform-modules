
resource "aws_cloudwatch_log_group" "vpc_logging" {
  name = "${var.vpc_name}-flow-logs"
}

//Copied straight from https://www.terraform.io/docs/providers/aws/r/flow_log.html and renamed.
//because I'm lazy.
resource "aws_iam_role" "logging" {
  name = "${var.vpc_name}-flow-logs"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "logging" {
  name = "${var.vpc_name}-flow-logs"
  role = "${aws_iam_role.logging.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}