
variable "vpc_name" {
  default = "new_vpc"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
  description = "The main IP block to assign to the VPC"
}

variable "usage" {
  default = "dev"
  description = "The usage level for the VPC"
}

variable "vpc_flow_log_level" {
  default = "ALL"
  description = "The level of flow logs to collect."
}

variable "assign_secondary_block" {
  type    = bool
  default = false

  description = "whether or not to assign a secondary VPC CIDR block"
}

variable "vpc_secondary_cidr_block" {
  default = "10.1.0.0/16"

  description = "if assign_secondary_block is set to true, then the block to set"
}

variable "public_subnet_count" {
  default = 0
  description = "How many public subnets to create in the VPC"
}

variable "public_subnet_offset" {
  default = 100
  description = "How many netnum to offset from 0 when creating private subnets. This number should be larger than the number of public subnets created."
}

variable "private_subnet_count" {
  default = 4
  description = "How many private subnets to create in the VPC"
}

variable "subnet_cidr_bit_offset" {
  default = 4
  description = "How many bits to reserve for networks when using the cidrsubnet function. CAUTION: If you change this after applying tf, all subnets will be destroyed and rebuilt."
}

