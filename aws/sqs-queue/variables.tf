variable "queue_name" {
  description = "The name to use for the queue"
}

//Most variables below this line are a passthrough from terraform's defaults. They're required to be repeated here so that
// we can expose them all through the module.
variable "delay_seconds" {
  description = "The amount of time every message will wait prior to delivery."
  default = 0
}
variable "max_message_size" {
  description = "How large the message is allowed to be"
  default = 262144
}
variable "message_retention_seconds" {
  description = "How long to hold onto the message"
  default = 345600
}
variable "receive_wait_time_seconds" {
  description = "How long to wait for a call to return if a message isn't present. This default is different than Terraforms because long polling is cheaper generally in a low volume situation"
  default = 20
}
variable "usage" {
  description = "The environment level"
}
