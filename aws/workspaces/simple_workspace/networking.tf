
module "create_vpc" {
  source = "../../networking/vpc-with-subnets"

  vpc_name = "${var.workspaces_name}-vpc"
  vpc_cidr_block =  var.cidr_block
  private_subnet_count = var.private_subnets
  usage = var.usage
}

module "create_inet_gateway" {
  source = "../../networking/internet-gateway"

  usage = var.usage
  vpc_id = module.create_vpc.vpc_id
}