variable "workspaces_name" {
  default = "my_workspaces"
}
variable "cidr_block" {
  default = "10.0.0.0/16"
  description = "The main IP block to assign to the VPC"
}
variable "private_subnets" {
  default = 2
}
variable "usage" {
  default = "dev"
  description = "the usage level of the environment"
}
