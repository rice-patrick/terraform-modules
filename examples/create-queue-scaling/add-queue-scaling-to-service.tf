
provider "aws" {
  region  = "us-east-1"
}

module "lambda_custom_metric" {
  # Reference by Git URL so that these examples work elsewhere if someone wants to use them.
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/queue-based-scaling/ecs-service/"

  function_name = ""

  ecs_cluster_name = ""
  ecs_service_name = ""
  sqs_queue_url = ""
}