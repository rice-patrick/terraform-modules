# Creating a Database Migration Service Cluster
This terraform is an example of using Amazon's Database Migration Service to migrate data from
RDS to Amazon Redshift. The modules used here assume that the source database's password is stored within
a parameter store entry. The Redshift password can be stored either in parameter store, or availabe within
the module.

_Note_: DMS resources created in Amazon take a while to create. You should expect this example to take approximately
14 minutes to create the resources, and the same to destroy them once finished.

## Resources Created

* Amazon Redshift Cluster
  * This cluster will be created from scratch using the [module located in this repository](../../aws/databases/redshift/).
* Amazon Database Migration Service Cluster, spread across 2 Availability Zones
* Source database endpoint for Amazon's DMS
* Target database endpoint for Amazon's DMS
  * These 3 resources are created from scratch using the [module located in this repository](../../aws/databases/dms-cluster/).

These resources are linked together with security groups which this example adds ingress rules for. 

## What's next?

Once a DMS cluster is created, "jobs" are run on that cluster that migrate data from one database to another. These jobs can
either be created through the console for testing purpose, or [via terraform](https://www.terraform.io/docs/providers/aws/r/dms_replication_task.html)
using the source and target endpoints created as part of the DMS Cluster module.