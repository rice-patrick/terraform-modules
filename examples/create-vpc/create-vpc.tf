
provider "aws" {
  region  = "us-east-1"
}

module "my_vpc" {
  # Reference by Git URL so that these examples work elsewhere if someone wants to use them.
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/networking/vpc-with-subnets/"

  assign_secondary_block = "true"
}